/* global CKEDITOR */
;(function() {
  // Polyfill from https://developer.mozilla.org/en/docs/Web/API/Element/matches
  if (!Element.prototype.matches) {
    Element.prototype.matches =
        Element.prototype.matchesSelector ||
        Element.prototype.mozMatchesSelector ||
        Element.prototype.msMatchesSelector ||
        Element.prototype.oMatchesSelector ||
        Element.prototype.webkitMatchesSelector ||
        function(s) {
            var matches = (this.document || this.ownerDocument).querySelectorAll(s),
                i = matches.length;
            while (--i >= 0 && matches.item(i) !== this) {}
            return i > -1;
        };
  }

  function runInitialisers() {
    if (!window.ClassicEditor) {
      setTimeout(runInitialisers, 100);
      return;
    }

    initialiseCKEditor5();
  }

  if (document.readyState != 'loading' && document.body) {
    document.addEventListener('DOMContentLoaded', initialiseCKEditor5);
    runInitialisers();
  } else {
    document.addEventListener('DOMContentLoaded', runInitialisers);
  }

  function initialiseCKEditor5() {
    
    var textareas = Array.prototype.slice.call(document.querySelectorAll('textarea[data-type=ckeditortype]'));
    for (var i=0; i<textareas.length; ++i) {
      var ta = textareas[i];
      if (ta.getAttribute('data-processed') == '0' && ta.id.indexOf('__prefix__') == -1) {
        ta.setAttribute('data-processed', '1');

        ClassicEditor
          .create( ta, JSON.parse(ta.getAttribute('data-config')) )
          .then( editor => {
            console.log( Array.from( editor.ui.componentFactory.names() ) );
          } )
          .catch( error => {
            console.error( error );
          } );

      }
    }
  }

}());

		
		