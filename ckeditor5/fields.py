from django import forms
from django.db import models

from .widgets import CKEditorWidget


class RichTextField(models.TextField):

    def __init__(self, *args, **kwargs):
        self.config_name = kwargs.pop("config_name", "default")
        self.div_css = kwargs.pop("div_css", None)
        super().__init__(*args, **kwargs)

    def formfield(self, **kwargs):
        defaults = {
            'form_class': self._get_form_class(),
            'config_name': self.config_name,
            'div_css': self.div_css
        }
        defaults.update(kwargs)
        return super().formfield(**defaults)

    @staticmethod
    def _get_form_class():
        return RichTextFormField


class RichTextFormField(forms.fields.CharField):

    def __init__(self, config_name='default', div_css=None, *args, **kwargs):
        kwargs.update({'widget': CKEditorWidget(config_name=config_name, div_css=div_css)})
        super().__init__(*args, **kwargs)
