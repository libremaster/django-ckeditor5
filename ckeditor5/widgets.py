# django
from django import forms
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.core.serializers.json import DjangoJSONEncoder
from django.utils.encoding import force_text
from django.utils.functional import Promise
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe
from django.utils.translation import get_language
from django.urls import reverse
from django.forms.widgets import get_default_renderer
from django.forms.utils import flatatt

# config
from .configs import DEFAULT_CONFIG

# widgets
class LazyEncoder(DjangoJSONEncoder):

    def default(self, obj):
        if isinstance(obj, Promise):
            return force_text(obj)
        return super().default(obj)

json_encode = LazyEncoder().encode

class CKEditorWidget(forms.Textarea):
    """
    Widget providing CKEditor for Rich Text Editing.
    Supports direct image uploads and embed.
    """

    class Media:
        js = (
            'ckeditor5/ckeditor/ckeditor.js',
            'ckeditor5/ckeditor/translations/'+get_language()+'.js',
            'ckeditor5/ckeditor-init.js'
        )

    def __init__(self, config_name='default', div_css=''):
        super().__init__()
        self.div_css = div_css

        # Setup config from defaults.
        self.config = DEFAULT_CONFIG.copy()

        # Try to get valid config from settings.
        configs = getattr(settings, 'CKEDITOR5_CONFIGS', None)
        if configs:
            if isinstance(configs, dict):
                # get default from settings
                if 'default' in configs:
                    self.config.update(configs['default'])
                # Make sure the config_name exists.
                if config_name in configs:
                    config = configs[config_name]
                    # Make sure the configuration is a dictionary.
                    if not isinstance(config, dict):
                        raise ImproperlyConfigured('CKEDITOR5_CONFIGS["%s"] \
                                setting must be a dictionary type.' %
                                                   config_name)
                    # Override defaults with settings config.
                    self.config.update(config)
            else:
                raise ImproperlyConfigured('CKEDITOR5_CONFIGS setting must be a\
                        dictionary type.')

    def render(self, name, value, attrs=None, renderer=None):
        context = self.get_context(name, value, attrs)
        if renderer is None:
            renderer = get_default_renderer()

        if value is None:
            value = ''

        self._set_config()

        context['widget']['attrs'].update({
            'data-processed': '0',
            'data-config': json_encode(self.config),
            'data-type': 'ckeditortype',
            'data-id': context['widget']['attrs']['id']
        })

        context.update({
            'value': conditional_escape(force_text(value)),
            'div_css': self.div_css
        })

        return mark_safe(renderer.render('ckeditor5/widget.html', context))


    def _set_config(self):
        self.config['language'] = get_language()
        self.config['simpleUpload'] = {
            'uploadUrl': reverse('ckeditor_upload')
        }