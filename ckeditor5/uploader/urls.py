from django.urls import path
from django.contrib.admin.views.decorators import staff_member_required

from . import views

urlpatterns = [
    path('upload/', staff_member_required(views.ImageUploadView.as_view()), name='ckeditor_upload'),
]
